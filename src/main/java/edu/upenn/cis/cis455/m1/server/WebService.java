/**
 * CIS 455/555 route-based HTTP framework
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m1.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Filter;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.server.HttpSession;

public class WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);

    protected String ipAddress = "0.0.0.0";
    protected int port = 45555;
    protected int threadPoolSize = 16;
    public String staticFileLocation = "./www";

    public HttpTaskQueue httpTaskQueue;

    public List<HttpWorker> workers = new ArrayList<>();
    public List<Thread> workerThreads = new ArrayList<>();

    protected HttpListener listener;
    protected Thread listenerThread;

    public HashMap<String, List<String>> routePaths = new HashMap<>();
    public HashMap<String, List<Route>> routes = new HashMap<>();
    public List<String> beforeFilterPaths = new ArrayList<>();
    public List<Filter> beforeFilters = new ArrayList<>();
    public List<String> afterFilterPaths = new ArrayList<>();
    public List<Filter> afterFilters = new ArrayList<>();
    public Filter beforeAllFilter = null;
    public Filter afterAllFilter = null;

    private ConcurrentMap<String, HttpSession> sessions = new ConcurrentHashMap<>();

    /**
     * Launches the Web server thread pool and the listener
     */
    public void start() {
        logger.info(
            "Serving with params: " + 
            "ip address: " + ipAddress + ", " + 
            "port " + port + ", " + 
            "threads: " + threadPoolSize + ", " + 
            "static file location: " + staticFileLocation
        );

        // Create an HttpTaskQueue. 
        httpTaskQueue = new HttpTaskQueue();

        // Create threadPoolSize of HttpWorker. 
        for (int i = 0; i < threadPoolSize; i++) {
            HttpWorker w = new HttpWorker(this);
            this.workers.add(w);
            Thread t = new Thread(w);
            this.workerThreads.add(t);
            t.start();
        }

        // Create a thread and run an HttpListener. 
        this.listener = new HttpListener(this.ipAddress, this.port, this.httpTaskQueue);
        this.listenerThread = new Thread(this.listener);
        this.listenerThread.start();
    }

    protected Thread shutdown() {
        class ServiceShutdown implements Runnable {
            WebService service;

            public ServiceShutdown(WebService service) {
                this.service = service;
            }

            @Override
            public void run() {
                // Stop the http listener. 
                this.service.listener.stop();
                try {
                    this.service.listenerThread.join(10000); // Wait 10 seconds. 
                } catch (InterruptedException e) {
                }
                if (this.service.listenerThread.isAlive()) {
                    logger.warn("Listener thread is unable to end gracefully in 10 seconds, interrupting. ");
                    this.service.listenerThread.interrupt();
                }
        
                // Wait until all tasks are getting processed. 
                while (this.service.httpTaskQueue.size() > 0) {
                    // Wait until 0. 
                    try {
                        this.wait(200); // Wait 2/10 seconds. 
                    } catch (InterruptedException e) {
                    }
                }
        
                // Stop the workers. 
                for (int i = 0; i < this.service.workers.size(); i++) {
                    this.service.workers.get(i).stop();
                }
                for (int i = 0; i < this.service.workers.size(); i++) {
                    try {
                        this.service.workerThreads.get(i).join(10000); // Wait 10 seconds. 
                    } catch (InterruptedException e) {
                    }
                    if (this.service.workerThreads.get(i).isAlive()) {
                        logger.warn("A worker is unable to end gracefully in 10 seconds, interrupting. ");
                        this.service.workerThreads.get(i).interrupt();
                    }
                }
            }
        }
        
        Thread shutdownThread = new Thread(new ServiceShutdown(this));
        shutdownThread.start();
        return shutdownThread;
    }

    /**
     * Gracefully shut down the server
     */
    public void stop() {
        Thread shutdownThread = this.shutdown();
        try {
            shutdownThread.join();
        } catch (InterruptedException e) {
        }
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     */
    public void awaitInitialization() {
        logger.info("Initializing server");
        start();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
        this.staticFileLocation = directory;
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {
        this.port = port;
    }

    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {
        this.threadPoolSize = threads;
    }

    public HttpSession createSession() {
        HttpSession sess = new HttpSession();
        this.sessions.put(sess.id(), sess);
        return sess;
    }

    public HttpSession getSession(String id) {
        return this.sessions.get(id);
    }

    public HttpSession deleteSession(String id) {
        return this.sessions.remove(id);
    }

}
