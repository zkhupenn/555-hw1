package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.HttpSession;

public class HttpRequest extends Request {
    final static Logger logger = LogManager.getLogger(HttpRequest.class);

    WebService service;

    int port;
    String target;
    String body = "";
    Map<String, String> headers = new HashMap<>();
    Map<String, String> params = new HashMap<>(); // Params in path that correspondes to specific routes. 
    Map<String, List<String>> queryParams = new HashMap<>(); // Query params in URL. 
    Map<String, Object> attributes = new HashMap<>(); // Attributes for developers to pass around during one request-response cycle (between routes and filters). 
    Map<String, String> cookies = new HashMap<>();
    HttpSession sess = null;
    List<String> splat = new ArrayList<>();

    public HttpRequest(Socket socket, WebService service) throws HaltException {
        this.service = service;

        try {
            socket.setSoTimeout(5000);
            this.port = socket.getLocalPort();

            InputStream in = new BufferedInputStream(socket.getInputStream());

            String remoteIp = socket.getRemoteSocketAddress().toString();
            this.target = HttpParsing.parseRequest(remoteIp, in, this.headers, this.queryParams);

            // https://piazza.com/class/ky8s53l7b7i3j4?cid=350
            String protocolVer = this.headers.get("protocolVersion");
            // Check HTTP protocol errors. 
            if (protocolVer.startsWith("http")) {
                // Lowercase HTTP protocol. Return 400. 
                throw new HaltException(400, "Bad Request");
            }
            if (protocolVer.indexOf("HTTP/") == 0 && !(protocolVer.equals("HTTP/1") || protocolVer.equals("HTTP/1.0") || protocolVer.equals("HTTP/1.1"))) {
                // Unsupported HTTP version. 
                throw new HaltException(505, "HTTP Version Not Supported");
            }
            if (protocolVer.equals("HTTP/1.1") && !this.headers.containsKey("host")) {
                // HTTP/1.1 but no host header. 
                throw new HaltException(400, "Bad Request");
            }

            // Fix correct pathInfo, which can be an absolute URL. 
            // Should be able to handle absolute URLs. Ex: GET http://localhost:45555/test.html HTTP/1.
            Pattern pattern = Pattern.compile("^https?://[^/]+");
            Matcher matcher = pattern.matcher(this.headers.getOrDefault("pathInfo", "/"));
            if (matcher.find()) {
                this.headers.put("pathInfo", this.headers.getOrDefault("pathInfo", "/").substring(matcher.group(0).length()));
            }
            if (this.headers.getOrDefault("pathInfo", "/").equals("")) {
                this.headers.put("pathInfo", "/");
            }

            try {
                // Should have returned 304 for request with future If-Modified-Since
                SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss zzz");
                if (this.headers.containsKey("if-modified-since")) {
                    Date ifModifiedSinceDate = dateFormat.parse(this.headers.get("if-modified-since"));
                    if (ifModifiedSinceDate.after(new Date(System.currentTimeMillis()))) {
                        throw new HaltException(304, "Not Modified");
                    }
                }
                
                // Should have returned 412 for request with past If-Unmodified-Since
                if (this.headers.containsKey("if-unmodified-since")) {
                    Date isUnmodifiedSinceDate = dateFormat.parse(this.headers.get("if-unmodified-since"));
                    if (new Date(System.currentTimeMillis()).after(isUnmodifiedSinceDate)) {
                        throw new HaltException(412, "Precondition Failed");
                    }
                }
            } catch (ParseException e) {
                throw new HaltException(400, "Bad Request");
            }

            // Get body. 
            if ("chucked".equals(this.headers("Transfer-Encoding"))) {
                try {
                    // Chucked encoding. 
                    InputStreamReader reader = new InputStreamReader(in);
                    BufferedReader inReader = new BufferedReader(reader);
                    StringBuilder bodyBuilder = new StringBuilder(); 
        
                    int contentLength = (int) Long.parseLong(inReader.readLine(), 16);

                    while (contentLength > 0) {
                        char[] buffer = new char[512];
        
                        int numRead = 0;
                        int totalRead = 0;
        
                        while (totalRead < contentLength && numRead != -1) {
                            numRead = inReader.read(buffer, 0, Math.min(contentLength - totalRead, 512));
                            bodyBuilder.append(buffer, 0, numRead);
                            totalRead += numRead;
                        }
                        inReader.readLine();

                        contentLength = (int) Long.parseLong(inReader.readLine(), 16);
                    }
                    this.body = bodyBuilder.toString();

                    // Read trailer headers. 
                    String line = inReader.readLine();
                    String lastKey = null;
                    while (line != null && !line.trim().isEmpty()) {
                        int p = line.indexOf(':');
                        if (p >= 0) {
                            lastKey = line.substring(0, p).trim().toLowerCase(Locale.US);
                            this.headers.put(lastKey, line.substring(p + 1).trim());
                        } else if (lastKey != null && line.startsWith(" ") || line.startsWith("\t")) {
                            String newPart = line.trim();
                            this.headers.put(lastKey, this.headers.get(lastKey) + newPart);
                        }
                        line = inReader.readLine();
                    }
                    logger.info(this.headers.toString());
                    
                } catch (HaltException e) {
                    throw e;
                } catch (Exception e) {
                    throw new HaltException(400, "Bad Request");
                }
            } else {
                int contentLength = this.contentLength();
                if (contentLength > 0) {
                    InputStreamReader reader = new InputStreamReader(in);
                    BufferedReader inReader = new BufferedReader(reader);
    
                    StringBuilder bodyBuilder = new StringBuilder();
                    char[] buffer = new char[512];
    
                    int numRead = 0;
                    int totalRead = 0;
    
                    while (totalRead < contentLength && numRead != -1) {
                        numRead = inReader.read(buffer);
                        bodyBuilder.append(buffer, 0, numRead);
                        totalRead += numRead;
                    }
                    this.body = bodyBuilder.toString();
                }
            }

            // Parse cookie. 
            if (this.headers.containsKey("cookie")) {
                String cookieStr = this.headers.get("cookie");
                for (String c : cookieStr.split(";")) {
                    String[] aCookie = c.split("=");
                    this.cookies.put(aCookie[0].strip(), aCookie[1].strip());
                }
            }

            // If POST and Content-Type: application/x-www-form-url-encoded, parse body data and put into queryParams
            if (this.requestMethod().equals("post") && this.contentType().equals("application/x-www-form-urlencoded")) {
                HttpParsing.decodeParms(this.body(), this.queryParams);
            }

            // Load sessions from service with JSESSIONID in cookies. 
            if (this.cookies.containsKey("JSESSIONID")) {
                String sessionId = this.cookie("JSESSIONID");
                this.sess = this.service.getSession(sessionId);
            }

        } catch (IOException e) {
            logger.error("Failed to read request. ");
            logger.error(this.headers.toString());
            logger.error(this.body.toString());
            e.printStackTrace();
            throw new HaltException(400, "Bad Request");
        }
    }

    @Override
    public String requestMethod() {
        return this.headers.get("method").toLowerCase();
    }

    @Override
    public String host() {
        return this.headers.getOrDefault("host", "");
    }

    @Override
    public String userAgent() {
        return this.headers.getOrDefault("user-agent", null);
    }

    @Override
    public int port() {
        return this.port;
    }

    @Override
    public String pathInfo() {
        return this.headers.getOrDefault("pathInfo", "/");
    }

    @Override
    public String url() {
        return this.protocol() + "://" + this.headers.get("host") + this.target;
    }

    @Override
    public String uri() {
        return this.protocol() + "://" + this.headers.get("host") + this.pathInfo();
    }

    @Override
    public String protocol() {
        return "http";
    }

    @Override
    public String contentType() {
        return this.headers.getOrDefault("content-type", "");
    }

    @Override
    public String ip() { // IP of client. 
        return this.headers.getOrDefault("remote-addr", "");
    }

    @Override
    public String body() {
        return this.body;
    }

    @Override
    public int contentLength() {
        return Integer.parseInt(this.headers.getOrDefault("content-length", "0"));
    }

    @Override
    public String headers(String name) {
        return this.headers.getOrDefault(name.toLowerCase(), null);
    }

    @Override
    public Set<String> headers() {
        return this.headers.keySet();
    }

    @Override
    public Session session() {
        // Check if the associated session has expired, then it should create and return a new session. 
        if (this.sess != null && !this.sess.isValid()) {
            // Delete session from server. 
            this.service.deleteSession(this.sess.id());
            this.sess = null;
        }
        if (this.sess == null) {
            this.sess = this.service.createSession();
        }
        return this.sess;
    }

    @Override
    public Session session(boolean create) {
        if (create) {
            // Always create a new session irrespective of whether the current session is active or expired. 
            // https://piazza.com/class/ky8s53l7b7i3j4?cid=601
            this.sess = this.service.createSession();
        } else {
            // It should return null if the current session has expired. 
            if (this.sess != null && !this.sess.isValid()) {
                // Delete session from server. 
                this.service.deleteSession(this.sess.id());
                this.sess = null;
            }
        }
        return this.sess;
    }

    @Override
    public Map<String, String> params() {
        return params;
    }

    @Override
    public String queryParams(String param) {
        List<String> p = this.queryParams.get(param);
        if (p == null) {
            return null;
        }
        return p.toString();
    }

    @Override
    public List<String> queryParamsValues(String param) {
        return this.queryParams.get(param);
    }

    @Override
    public Set<String> queryParams() {
        return this.queryParams.keySet();
    }

    @Override
    public String queryString() {
        return this.headers.get("queryString");
    }

    @Override
    public void attribute(String attrib, Object val) {
        this.attributes.put(attrib, val);
    }

    @Override
    public Object attribute(String attrib) {
        return this.attributes.get(attrib);
    }

    @Override
    public Set<String> attributes() {
        return this.attributes.keySet();
    }

    @Override
    public Map<String, String> cookies() {
        return this.cookies;
    }

    @Override
    public List<String> splat() {
        return this.splat;
    }

    @Override
    public boolean isKeepAlive() {
        return (("HTTP/1.1".equals(this.headers.get("protocolVersion")) && (!"close".equals(this.headers.get("connection")))) || 
        ("HTTP/1".equals(this.headers.get("protocolVersion")) && "keep-alive".equals(this.headers.get("connection"))));
    }

    
}
