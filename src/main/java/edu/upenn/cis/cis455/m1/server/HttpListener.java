package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {
    final static Logger logger = LogManager.getLogger(HttpListener.class);

    private boolean isListening = false;
    private String ipAddress = "0.0.0.0";
    private int port = 45555;

    HttpTaskQueue httpTaskQueue;

    public HttpListener(String ipAddress, int port, HttpTaskQueue httpTaskQueue) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.httpTaskQueue = httpTaskQueue;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(this.port, 50, InetAddress.getByName(this.ipAddress))) {
            this.isListening = true;
            logger.info("Started listening. ");
            serverSocket.setSoTimeout(500);

            while (this.isListening) {
                try {
                    Socket socket = serverSocket.accept();
                    logger.info("Got new connection. ");
                    
                    // Add to task queue. 
                    this.httpTaskQueue.offer(new HttpTask(socket));
                } catch (SocketTimeoutException e) {

                }
            }
            logger.info("Stopped listening");

        } catch (IOException e) {
            logger.error("Failed to open server socket at port " + this.port);
            logger.error(e.toString());
        }

    }

    public boolean isListening() {
        return isListening;
    }

    public void stop() {
        this.isListening = false;
    }
}
