package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.Configuration;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.FileRequestHandler;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m2.interfaces.Filter;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m2.interfaces.Route;

/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker implements Runnable {
    final static Logger logger = LogManager.getLogger(HttpWorker.class);

    protected boolean isRunning = false;
    private String urlServing = null;
    
    private WebService service;
    
    public HttpWorker(WebService service) {
        this.service = service;
    }

    @Override
    public void run() {
        this.isRunning = true;
        logger.info("Worker started. ");
        while (this.isRunning) {

            // Get one from http task queue. 
            HttpTask task = this.service.httpTaskQueue.poll();
            if (task == null) {
                continue;
            }
            Socket socket = task.getSocket();

            // Process request. 
            boolean isKeepAlive = true;
            while (isKeepAlive) {
                try {
                    Request req = handleRequest(socket);
                    this.urlServing = req.url();

                    logger.info("Handling " + this.urlServing);
                    
                    Response res = generateResponse(req);
                    logger.info("Response generated for " + this.urlServing);

                    // Send response. 
                    isKeepAlive = HttpIoHandler.sendResponse(socket, req, res);
                    logger.info("Response sent for " + this.urlServing);

                } catch (HaltException e) {
                    logger.info("Request failed to fullfill for " + this.urlServing);
                    isKeepAlive = HttpIoHandler.sendException(socket, null, e);
                } catch (Exception e) {
                    logger.error("Server failed to fullfill request for " + this.urlServing);
                    e.printStackTrace();
                    isKeepAlive = HttpIoHandler.sendException(socket, null, new HaltException(500, "Internal Server Error"));
                }
                this.urlServing = null;
            }
            
            // Close socket. 
            try {
                socket.close();
            } catch (IOException e) {
                logger.error("A Socket refused to close. ");
            }
        }
        logger.info("Worker stopped. ");
    }

    private Request handleRequest(Socket socket) throws HaltException {
        // Process request. 
        Request req = new HttpRequest(socket, this.service);
        return req;
    }

    private Response generateResponse(Request req) throws HaltException {
        logger.info("Generating Response. ");

        // Process response. 
        Response res = new HttpResponse();

        
        // Pre-response header poprogate. 
        if ("chucked".equals(req.headers("transfer-encoding"))) {
            res.header("transfer-encoding", "chucked");
        }

        
        String path = req.pathInfo();

        // Special paths /control /shutdown. 
        if (req.requestMethod().equals("get") && path.equals("/control")) {
            StringBuilder controlHtml = new StringBuilder();
            controlHtml.append("<html><body>");
            controlHtml.append("<a href='/shutdown'>shutdown</a><br />");

            for (int i = 0; i < this.service.workerThreads.size(); i++) {
                controlHtml.append(this.service.workerThreads.get(i).getId() + " ");

                String url = this.service.workers.get(i).urlServing();
                if (url.equals("")) {
                    controlHtml.append("waiting");
                } else {
                    controlHtml.append(url);
                }

                controlHtml.append("<br />");
            }

            // https://piazza.com/class/ky8s53l7b7i3j4?cid=530
            LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
            Configuration config = ctx.getConfiguration();
            Map<String, Appender> appenders = config.getAppenders();
            String fileName = null;
            for (String name: appenders.keySet()) {
                Appender appender = appenders.get(name);
                if (appender instanceof FileAppender) {
                    fileName = ((FileAppender) appender).getFileName();
                    break;
                }
            }
            if (fileName != null) {
                logger.info(fileName);
                Path log = Paths.get(fileName).toAbsolutePath();
                try {
                    controlHtml.append("<pre>");
                    controlHtml.append(new String(Files.readAllBytes(log), "UTF-8"));
                    controlHtml.append("</pre>");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            
            controlHtml.append("</body></html>");
            res.type("text/html");
            res.body(controlHtml.toString());
    
        } else if (req.requestMethod().equals("get") && path.equals("/shutdown")) {
            this.service.shutdown();
            res.body("Shutting Down");

        } else {
            // Set response. 
            try {
                this.populateResponse(req, res);
            } catch (HaltException e) {
                throw e;
            } catch (Exception e) {
                e.printStackTrace();
                throw new HaltException(500, "Internal Server Error");
            }
        }

        // Copy session id cookie from req to res if valid. 
        if (req.session(false) != null) {
            res.cookie("JSESSIONID", req.session(false).id());
        }

        // Post-response header propagate. 
        // Persistent HTTP. 
        if (req.isKeepAlive()) {
            // Keep alive. 
            res.header("connection", "keep-alive");
        }

        return res;
    }

    public boolean pathMatchGeneralWildcard(String template, String path, Map<String, String> params, List<String> splatList) {
        if (template.startsWith("/")) {
            template = template.substring(1);
        }
        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        if (template.length() == 0 && path.length() != 0) {
            return false;
        }
        if (template.length() == 0 && path.length() == 0) {
            return true;
        }
        if (template.length() != 0 && path.length() == 0) {
            return false;
        }
        
        String curTemplateComponent;
        String curPathComponent;
        String nextTemplate;
        String nextPath;

        int aTSlash = template.indexOf("/");
        if (aTSlash == -1) {
            curTemplateComponent = template;
            nextTemplate = "";
        } else {
            curTemplateComponent = template.substring(0, aTSlash);
            nextTemplate = template.substring(aTSlash + 1);
        }
        int aPSlash = path.indexOf("/");
        if (aPSlash == -1) {
            curPathComponent = path;
            nextPath = "";
        } else {
            curPathComponent = path.substring(0, aPSlash);
            nextPath = path.substring(aPSlash + 1);
        }

        if (curTemplateComponent.charAt(0) == ':') {
            params.put(curTemplateComponent, curPathComponent);
            
            if (!pathMatchGeneralWildcard(nextTemplate, nextPath, params, splatList)) {
                params.remove(curTemplateComponent);
                return false;
            }
            return true;
        } else if (curTemplateComponent.equals("*")) {
            String last = splatList.get(splatList.size() - 1);
            if (last.length() == 0) {
                last = curPathComponent;
            } else {
                last = last + "/" + curPathComponent;
            }
            splatList.set(splatList.size() - 1, last);

            splatList.add("");
            logger.info(splatList.toString());
            if (pathMatchGeneralWildcard(nextTemplate, nextPath, params, splatList)) {
                return true;
            }
            splatList.remove(splatList.size() - 1);

            return pathMatchGeneralWildcard(template, nextPath, params, splatList);
        } else if (curTemplateComponent.equals(curPathComponent)) {
            return pathMatchGeneralWildcard(nextTemplate, nextPath, params, splatList);
        } else {
            return false;
        }
    }

    public boolean pathMatches(String template, Request req) {
        String path = req.pathInfo();

        if (!template.startsWith("/")) {
            template = "/" + template;
        }
        if (!path.startsWith("/")) {
            path = "/" + path;
        }

        String[] tempComp = template.split("/");
        String[] pathComp = path.split("/");
        
        Map<String, String> params = new HashMap<>();
        List<String> splatList = new ArrayList<>();

        if (tempComp.length != pathComp.length) {
            // General * matching here. 
            splatList.add("");
            if (!pathMatchGeneralWildcard(template, req.pathInfo(), params, splatList)) {
                return false;
            }
            splatList.remove(splatList.size() - 1);
        } else {
            for (int i = 0; i < tempComp.length; i++) {
                if (tempComp[i].length() > 0 && tempComp[i].charAt(0) == ':') {
                    params.put(tempComp[i].toLowerCase(), pathComp[i]);
                } else if (tempComp[i].equals("*")) {
                    splatList.add(pathComp[i]);
                } else if (!tempComp[i].equals(pathComp[i])) {
                    return false;
                }
            }
        }

        req.params().clear();
        req.params().putAll(params);
        req.splat().clear();
        req.splat().addAll(splatList);
        
        return true;
    }

    public void populateResponse(Request req, Response res) throws HaltException, Exception {
        logger.info("Populating Response. ");
        // The most before filter. 
        if (this.service.beforeAllFilter != null) {
            this.service.beforeAllFilter.handle(req, res);
        }

        
        // Check matches with before filters. 
        List<String> beforeFilterPaths = this.service.beforeFilterPaths;
        List<Filter> beforeFilters = this.service.beforeFilters;

        for (int i = 0; i < beforeFilterPaths.size(); i++) {
            if (pathMatches(beforeFilterPaths.get(i), req)) {
                beforeFilters.get(i).handle(req, res);
            }
        }
        

        // Check route matches. 
        boolean foundRoute = false;
        List<String> routePaths = this.service.routePaths.get(req.requestMethod());
        List<Route> routes = this.service.routes.get(req.requestMethod());

        for (int i = 0; i < routePaths.size(); i++) {
            if (pathMatches(routePaths.get(i), req)) {
                Object resBody = routes.get(i).handle(req, res);
                if (resBody != null) {
                    res.body(resBody.toString());
                }
                foundRoute = true;
                break; // Only match the first route. 
            }
        }

        if (!foundRoute && req.requestMethod().equals("get")) { // Default serves file. 
            FileRequestHandler.getFile(this.service.staticFileLocation, req, res);
        } else if (!foundRoute) {
            throw new HaltException(501, "Not Implemented");
        }
        

        // Check matches with before filters. 
        List<String> afterFilterPaths = this.service.afterFilterPaths;
        List<Filter> afterFilters = this.service.afterFilters;

        for (int i = 0; i < afterFilterPaths.size(); i++) {
            if (pathMatches(afterFilterPaths.get(i), req)) {
                afterFilters.get(i).handle(req, res);
            }
        }


        // The most after filter. 
        if (this.service.afterAllFilter != null) {
            this.service.afterAllFilter.handle(req, res);
        }
    }

    public void stop() {
        this.isRunning = false;
    }

    public String urlServing() {
        return this.urlServing == null ? "" : new String(this.urlServing);
    }
}
