package edu.upenn.cis.cis455.m1.server;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
    private Queue<HttpTask> q = new LinkedList<>();

    /**
     * This function adds the Socket provided to the queue. 
     * 
     * @param s The Socket to be added. 
     * @return  <code>true</code> if it successfully adds to queue. 
     *          <code>false</code> if failed. 
     */
    public boolean offer(HttpTask t) {
        boolean succ = false;
        synchronized(q) {
            succ = q.offer(t);
        }
        return succ;
    }

    /**
     * This function gets and removes a Socket from the queue. 
     * 
     * @return  The first socket from the queue. 
     */
    public HttpTask poll() {
        HttpTask t = null;
        synchronized(q) {
            t = q.poll();
        }
        return t;
    }

    /**
     * This function returns the size of the queue. 
     * 
     * @return  The size of the queue. 
     */
    public int size() {
        int s;
        synchronized(q) {
            s = q.size();
        }
        return s;
    }
}
