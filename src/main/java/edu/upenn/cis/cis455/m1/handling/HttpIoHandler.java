package edu.upenn.cis.cis455.m1.handling;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m1.server.HttpResponse;

/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);

    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) {
        // Request request may be null. 

        Response response = new HttpResponse();
        response.status(except.statusCode());
        
        try {
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            // Output response. 
            out.write((
                "HTTP/1.1 " + response.status() + " " + except.body() + "\r\n" + 
                response.getHeaders()
            ).getBytes());

            byte[] body = response.bodyRaw();
            if (body != null) {
                out.write(body);
            }
            
        } catch (IOException e) {
            logger.error("Failed to write to socket");
            return false;
        }
        if (response.status() == 408) {
            // Keep-alive request timeout. 
            return false;
        }
        return request == null ? false : request.isKeepAlive();
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     */
    public static boolean sendResponse(Socket socket, Request request, Response response) {
        // Process response. 
        try {
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            // Output response. 
            out.write((
                "HTTP/1.1 " + response.status() + " OK\r\n" + 
                response.getHeaders()
            ).getBytes());

            if (response.getHeaders().toLowerCase().contains("transfer-encoding: chucked")) {
                int sent = 0;
                byte[] body = response.bodyRaw();
                while (body != null && sent < body.length) {
                    int toSend = Math.min(512, body.length - sent);
                    out.write((Integer.toHexString(toSend) + "\r\n").getBytes());
                    out.write(body, sent, toSend);
                    out.write("\r\n".getBytes());
                    sent += toSend;
                }
            } else {
                byte[] body = response.bodyRaw();
                if (body != null) {
                    out.write(body);
                }
            }
            
        } catch (IOException e) {
            logger.error("Failed to write to socket");
            return false;
        }
        if (response.status() == 408) {
            // Keep-alive request timeout. 
            return false;
        }
        return request == null ? false : request.isKeepAlive();
    }
}
