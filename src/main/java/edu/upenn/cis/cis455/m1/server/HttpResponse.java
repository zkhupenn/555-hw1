package edu.upenn.cis.cis455.m1.server;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;

import edu.upenn.cis.cis455.m2.interfaces.Response;

public class HttpResponse extends Response {
    Map<String, String> headers = new HashMap<>();
    Map<String, String> cookies = new HashMap<>();
    Map<String, String> cookiesWithPath = new HashMap<>();

    @Override
    public String getHeaders() {
        // Return headers. 
        StringBuilder headerStr = new StringBuilder();
        
        if (this.bodyRaw() != null) {
            this.headers.put("content-length", Integer.toString(this.bodyRaw().length));
        }
        
        if (this.type() == null) {
            this.type("text/plain");
        }
        this.headers.put("content-type", this.type());

        // Date in header except 100 level responses. 
        // https://piazza.com/class/ky8s53l7b7i3j4?cid=313
        if (this.status() / 100 != 1) {
            SimpleDateFormat f = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
            f.setTimeZone(TimeZone.getTimeZone("GMT"));
            this.headers.put("Date", f.format(Calendar.getInstance().getTime()));
        }

        // Add server agents. 
        this.headers.put("Server", "CIS555/m2");

        // Get the headers into a string. 
        for (Entry<String, String> e : headers.entrySet()) {
            headerStr.append(e.getKey().toLowerCase() + ": " + e.getValue() + "\r\n");
        }

        // Add cookies. 
        for (Entry<String, String> c : cookies.entrySet()) {
            headerStr.append("Set-Cookie: " + c.getKey() + "=" + c.getValue() + "\r\n");
        }
        for (Entry<String, String> c : cookiesWithPath.entrySet()) {
            String cookieName = c.getKey().split(";")[0];
            headerStr.append("Set-Cookie: " + cookieName + "=" + c.getValue() + "\r\n");
        }

        headerStr.append("\r\n");

        return headerStr.toString();
    }

    @Override
    public void header(String header, String value) {
        this.headers.put(header, value);
    }

    @Override
    public void redirect(String location) {
        this.status(302);
        this.header("Location", location);
    }

    @Override
    public void redirect(String location, int httpStatusCode) {
        this.status(httpStatusCode);
        this.header("Location", location);
    }

    @Override
    public void cookie(String name, String value) {
        this.cookies.put(name, value);
    }

    @Override
    public void cookie(String name, String value, int maxAge) {
        this.cookies.put(name, value + "; Max-Age=" + maxAge);
    }

    @Override
    public void cookie(String name, String value, int maxAge, boolean secured) {
        this.cookies.put(name, value + "; Max-Age=" + maxAge + (secured ? "; Secure" : ""));
    }

    @Override
    public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        this.cookies.put(name, value + "; Max-Age=" + maxAge + (secured ? "; Secure" : "") + (httpOnly ? "; HttpOnly" : ""));
    }

    @Override
    public void cookie(String path, String name, String value) {
        this.cookiesWithPath.put(name + ";" + path, value + "; Path=" + path);
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge) {
        this.cookiesWithPath.put(name + ";" + path, value + "; Path=" + path + "; Max-Age=" + maxAge);
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured) {
        this.cookiesWithPath.put(name + ";" + path, value + "; Path=" + path + "; Max-Age=" + maxAge + (secured ? "; Secure" : ""));
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        this.cookiesWithPath.put(name + ";" + path, value + "; Path=" + path + "; Max-Age=" + maxAge + (secured ? "; Secure" : "") + (httpOnly ? "; HttpOnly" : ""));
    }

    @Override
    public void removeCookie(String name) {
        this.cookies.remove(name);
    }

    @Override
    public void removeCookie(String path, String name) {
        this.cookiesWithPath.remove(name + ";" + path);
    }
    
}
