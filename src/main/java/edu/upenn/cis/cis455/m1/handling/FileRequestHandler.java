package edu.upenn.cis.cis455.m1.handling;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.interfaces.Response;
import edu.upenn.cis.cis455.m1.interfaces.Request;

public class FileRequestHandler {
    final static Logger logger = LogManager.getLogger(FileRequestHandler.class);
    
    public static void getFile(String staticFileLocation, Request req, Response res) throws HaltException {
        String path = req.pathInfo();

        // Check file access security. 
        Path parent = Paths.get(staticFileLocation).toAbsolutePath();
        Path child = Paths.get(staticFileLocation, path).toAbsolutePath();

        // File security. 
        if (!child.startsWith(parent)) {
            // File not accessible. 
            throw new HaltException(403, "Forbidden");
        }
        
        // File exist or not. 
        File file = child.toFile();
        if (!file.exists()) {
            // File not found. 
            throw new HaltException(404, "Not Found");
        }

        // Directory display. 
        if (file.isDirectory() && Paths.get(staticFileLocation, path, "index.html").toFile().exists()) {
            child = Paths.get(staticFileLocation, path, "index.html");
            file = child.toFile();
        }
        if (file.isDirectory()) {
            // Return directory. 
            /* If a directory was requested, the request should 
             * (1) look for index.html within that directory and 
             *     return that if it exists, and 
             * (2) if not, return a 404 error (handled above).
             */
            StringBuilder dirHtml = new StringBuilder();
            dirHtml.append("<html><body>");
            for (final File fileEntry : file.listFiles()) {
                ;
                dirHtml.append(
                    "<a href='" + 
                        Paths.get(path, fileEntry.getName()).toString() + 
                    "'>" + 
                        fileEntry.getName() + 
                    "</a><br />"
                );
            }
            dirHtml.append("</body></html>");
            res.type("text/html");
            res.body(dirHtml.toString());
            return;
        }

        // Get file and set the body of request. 
        try {
            // Set MIME type of response header. 
            res.type(Files.probeContentType(child));
            res.bodyRaw(Files.readAllBytes(child));
            return;
        } catch (IOException e) {
            throw new HaltException(500, "Internal Server Error");
        }
    }
}
