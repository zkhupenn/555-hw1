package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Initialization / skeleton class.
 * Note that this should set up a basic web server for Milestone 1.
 * For Milestone 2 you can use this to set up a basic server.
 * 
 * CAUTION - ASSUME WE WILL REPLACE THIS WHEN WE TEST MILESTONE 2,
 * SO ALL OF YOUR METHODS SHOULD USE THE STANDARD INTERFACES.
 * 
 * @author zives
 *
 */
public class WebServer {
    final static Logger logger = LogManager.getLogger(WebServer.class);
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);

        // Make sure you parse *BOTH* command line arguments properly
        if (args.length > 0) {
            try {
                int port = Integer.parseInt(args[0]);
                port(port);
            } catch(Exception e) {
                logger.error("Not a valid port");
            }
        }
        if (args.length > 1) {
            staticFileLocation(args[1]);
        }
        // threadPool(8);
        
        // All user routes should go below here...
        get("/hello", (req, res) -> "Hello!!!");
        get("hi", (req, res) -> "Hi!!!");
        get("/hi/:name", (req, res) -> "Hi " + req.params(":name"));
        get("/hi/:name/:age", (req, res) -> "Hi " + req.params(":age") + "-year-old " + req.params(":name"));
        get("/hello/*/:name", (req, res) -> "Hello " + req.params(":name") + " " + req.splat().toString());
        get("/asplat/*/s/*", (req, res) -> "a " + req.splat().toString());
        get("/bsplat/*/s", (req, res) -> "b " + req.splat().toString());
        get("/csplat/*/*/*", (req, res) -> "c " + req.splat().toString());
        get("/dsplat/*/*", (req, res) -> "d " + req.splat().toString());
        get("/aasplat/*/:s/*", (req, res) -> "aa " + req.splat().toString() + " " + req.params().toString());
        get("/bbsplat/*/:s", (req, res) -> "bb " + req.splat().toString() + " " + req.params().toString());
        get("/ccsplat/*/*/:s/*", (req, res) -> "cc " + req.splat().toString() + " " + req.params().toString());
        get("/ddsplat/*/*/:s", (req, res) -> "dd " + req.splat().toString() + " " + req.params().toString());
        post("/hello/:name/:age", (req, res) -> "Hello " + req.params(":name") + req.params(":age") + req.queryParams("apple"));
        before("/hello/*/*", (req, res) -> { req.attribute("beforehello", "hellooo!!"); });
        after("/hi", (req, res) -> { res.header("afterhi", "afterhi"); });
        after((req, res) -> { res.cookie("afterall", "apple"); });
        before((req, res) -> { res.header("allbefore", "allbefore"); });
        get("/login", (req, res) -> { req.session().attribute("isLoggedIn", "true"); req.session().attribute("n", 0); res.redirect("/page"); return "logged in"; });
        get("/page", (req, res) -> { req.session().attribute("n", ((Integer) req.session().attribute("n")) + 1); return req.session().attribute("isLoggedIn") + req.session().attribute("n").toString(); });
        get("/logout", (req, res) -> { req.session().invalidate(); return "logged out"; });
        get("/error", (req, res) -> { halt(501, "msggg"); return "no error"; });
        get("/fib/:n", (req, res) -> {
            logger.info("getting fib");
            long n = Long.parseLong(req.params(":n"));
            if (n <= 2) {
                return 1;
            }
            long a = 1;
            long b = 1;
            long nn = n - 2;
            boolean overflowed = false;
            while (nn > 0) {
                long c = a + b;
                a = b;
                b = c;
                nn--;
                if (a > b) {
                    overflowed = true;
                }
            }
            logger.info("done fib");
            return b + (overflowed ? " overflowed" : "");
        });
        // ... and above here. Leave this comment for the Spark comparator tool

        System.out.println("Waiting to handle requests!");
        awaitInitialization();


        newServer("anotherServer");
        port(46666);
        get("/hello", (req, res) -> "Hello From Another Server!!!");
        awaitInitialization();


        switchToServer("default");
        get("/afterswitch", (req, res) -> "Switched. ");
    }
}
