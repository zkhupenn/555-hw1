package edu.upenn.cis.cis455.m2.server;

import java.util.Calendar;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import edu.upenn.cis.cis455.m2.interfaces.Session;

public class HttpSession extends Session {

    String id = UUID.randomUUID().toString();
    long creationTime = Calendar.getInstance().getTime().getTime();
    long lastAccessedTime = Calendar.getInstance().getTime().getTime();
    boolean isValid = true;
    int maxInactiveInterval = 10 * 60; // In seconds. 

    ConcurrentMap<String, Object> attributes = new ConcurrentHashMap<>(); // There may be two requests with the same session. 

    @Override
    public String id() {
        this.access();
        return this.id;
    }

    @Override
    public long creationTime() {
        this.access();
        return this.creationTime;
    }

    @Override
    public long lastAccessedTime() {
        long last = this.lastAccessedTime;
        this.access();
        return last;
    }

    @Override
    public void invalidate() {
        this.access();
        this.isValid = false;
    }

    @Override
    public int maxInactiveInterval() {
        this.access();
        return this.maxInactiveInterval;
    }

    @Override
    public void maxInactiveInterval(int interval) {
        this.access();
        this.maxInactiveInterval = interval;
    }

    @Override
    public void access() {
        lastAccessedTime = Calendar.getInstance().getTime().getTime();
    }

    @Override
    public void attribute(String name, Object value) {
        this.access();
        this.attributes.put(name, value);
    }

    @Override
    public Object attribute(String name) {
        this.access();
        return this.attributes.get(name);
    }

    @Override
    public Set<String> attributes() {
        this.access();
        return this.attributes.keySet();
    }

    @Override
    public void removeAttribute(String name) {
        this.access();
        this.attributes.remove(name);
    }

    public boolean isValid() {
        return this.isValid && ((Calendar.getInstance().getTime().getTime() - this.lastAccessedTime) / 1000 < this.maxInactiveInterval);
    }
    
}
