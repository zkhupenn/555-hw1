package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

public class SampleWebApp {
    public static void main() {
        
        // Set the web server path to ./www
        staticFileLocation("./www");
        
        System.out.println("Here");
        
        // At this point, the web server should handle requests for static files from ./www
        // if you open your web browser to the Preview URL
        
        
        // Uncomment this for milestone 2: it will be a GET handler to a simple lambda function
        get("index.html", (request, response) -> "Hello World");

        
        get("/hello", (req, res) -> "Hello!!!");
        get("hi", (req, res) -> "Hi!!!");
        get("/hi/:name", (req, res) -> "Hi " + req.params(":name"));
        get("/hi/:name/:age", (req, res) -> "Hi " + req.params(":age") + "-year-old " + req.params(":name"));
        get("/hello/*/:name", (req, res) -> "Hello " + req.params(":name"));
        before("/hello/*/*", (req, res) -> { res.header("apple", "banana"); });
        get("/login", (req, res) -> { req.session().attribute("isLoggedIn", "true"); res.redirect("/page"); return "logged in"; });
        get("/page", (req, res) -> { return req.session().attribute("isLoggedIn"); });
        get("/logout", (req, res) -> { req.session().invalidate(); return "logged out"; });
        // ... and above here. Leave this comment for the Spark comparator tool

        System.out.println("Waiting to handle requests!");
        awaitInitialization();


        newServer("anotherServer");
        port(46666);
        get("/hello", (req, res) -> "Hello From Another Server!!!");
        awaitInitialization();
    }
}
