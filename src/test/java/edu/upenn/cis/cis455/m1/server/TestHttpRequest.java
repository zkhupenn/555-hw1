package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;

import org.apache.logging.log4j.Level;

public class TestHttpRequest {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }

    String sampleGetRequest = 
        "GET /a/b/hello.htm?q=x&v=12%200 HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: www.cis.upenn.edu\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
        "Connection: Keep-Alive\r\n\r\n";
    
    @Test
    public void testHttpRequestFunctions() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
            
        Request req = new HttpRequest(s, null);
        
        assertEquals("www.cis.upenn.edu", req.host());
        assertEquals(45555, req.port());
        assertEquals("host:8080", req.ip());
        assertEquals("get", req.requestMethod());
        assertEquals(0, req.contentLength());
        assertEquals("http://www.cis.upenn.edu/a/b/hello.htm", req.uri());
        assertEquals("http://www.cis.upenn.edu/a/b/hello.htm?q=x&v=12%200", req.url());
        assertEquals("/a/b/hello.htm", req.pathInfo());
        assertEquals("", req.contentType());
        assertEquals("http", req.protocol());
        assertEquals("Mozilla/4.0 (compatible; MSIE5.01; Windows NT)", req.userAgent());
    }
    
    @Test
    public void testHttpRequestHeaders() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
            
        Request req = new HttpRequest(s, null);
        
        assertEquals("Mozilla/4.0 (compatible; MSIE5.01; Windows NT)", req.headers("User-Agent"));
        assertEquals("www.cis.upenn.edu", req.headers("Host"));
        assertEquals("en-us", req.headers("Accept-Language"));
        assertEquals("gzip, deflate", req.headers("Accept-Encoding"));
        assertEquals("name1=value1; name2=value2; name3=value3", req.headers("Cookie"));
        assertEquals("Keep-Alive", req.headers("Connection"));

        String[] sarr = {
            "User-Agent".toLowerCase(), 
            "Host".toLowerCase(), 
            "Accept-Language".toLowerCase(), 
            "Accept-Encoding".toLowerCase(), 
            "Cookie".toLowerCase(), 
            "Connection".toLowerCase()
        };
        assertTrue(req.headers().containsAll(Arrays.asList(sarr)));
        
    }
    
    @Test
    public void testHttpRequestIfModifiedSinceError() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;

        // Should have returned 304 for request with future If-Modified-Since
        s = TestHelper.getMockSocket(
            "GET / HTTP/1\r\nIf-Modified-Since: Tue, 19 Jan 2038 03:14:07 GMT\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(304, e.statusCode());
        }
    }

    @Test
    public void testHttpRequestIfUnmodifiedSinceError() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;
        
        // Should have returned 412 for request with past If-Unmodified-Since
        s = TestHelper.getMockSocket(
            "GET / HTTP/1\r\nIf-UnModified-Since: Wed, 21 Oct 2015 07:28:00 GMT\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(412, e.statusCode());
        }
    }

    @Test
    public void testHttpRequestHttpProtocolErrors() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;
        
        // Success case. 
        s = TestHelper.getMockSocket(
            "GET / HTTP/1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
        } catch (HaltException e) {
            assertTrue(false);
        }
        
        // Lowercase HTTP
        s = TestHelper.getMockSocket(
            "GET / http/1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(400, e.statusCode());
        }
        
        // Lowercase HTTP
        s = TestHelper.getMockSocket(
            "GET / http/1.1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(400, e.statusCode());
        }
        
        // HTTP/1.1 no host. 
        s = TestHelper.getMockSocket(
            "GET / HTTP/1.1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(400, e.statusCode());
        }
        
        // HTTP/1.2 unsupported. 
        s = TestHelper.getMockSocket(
            "GET / HTTP/1.2\r\n\r\n", 
            byteArrayOutputStream
        );
        
        try {
            new HttpRequest(s, null);
            assertTrue(false);
        } catch (HaltException e) {
            assertEquals(505, e.statusCode());
        }
    }

    @Test
    public void testHttpRequestQueryParams() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;
        
        // Success case. 
        s = TestHelper.getMockSocket(
            "GET /?a=b&c=d HTTP/1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        Request req = new HttpRequest(s, null);
        assertEquals("[b]", req.queryParams("a"));
        assertEquals("b", req.queryParamsValues("a").get(0));
        assertEquals("[d]", req.queryParams("c"));
        assertEquals("d", req.queryParamsValues("c").get(0));
        assertEquals("a=b&c=d", req.queryString());
    }

    @Test
    public void testHttpRequestAttributes() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;
        
        // Success case. 
        s = TestHelper.getMockSocket(
            "GET /?a=b&c=d HTTP/1\r\n\r\n", 
            byteArrayOutputStream
        );
        
        Request req = new HttpRequest(s, null);

        req.attribute("abc", "def");
        assertEquals("def", req.attribute("abc"));
    }

    @Test
    public void testHttpRequestCookies() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s;
        
        // Success case. 
        s = TestHelper.getMockSocket(
            "GET /?a=b&c=d HTTP/1\r\nCookie: yummy_cookie=choco; tasty_cookie=strawberry\r\n\r\n", 
            byteArrayOutputStream
        );
        
        Request req = new HttpRequest(s, null);
        assertEquals("choco", req.cookies().get("yummy_cookie"));
        assertEquals("strawberry", req.cookies().get("tasty_cookie"));
    }

    
    @After
    public void tearDown() {}
}
