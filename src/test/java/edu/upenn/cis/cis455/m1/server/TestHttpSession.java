package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.util.Calendar;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.m2.server.HttpSession;

import org.apache.logging.log4j.Level;

public class TestHttpSession {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    @Test
    public void testHttpSession() throws IOException {
        HttpSession sess = new HttpSession();
        assertTrue(Calendar.getInstance().getTime().getTime() - sess.creationTime() < 1000);
        assertTrue(Calendar.getInstance().getTime().getTime() - sess.lastAccessedTime() < 1000);

        assertTrue(sess.maxInactiveInterval() == 600);
        sess.maxInactiveInterval(700);
        assertTrue(sess.maxInactiveInterval() == 700);

        sess.attribute("a", "b");
        assertEquals("b", sess.attribute("a"));

        sess.removeAttribute("a");
        assertNull(sess.attribute("a"));

        assertTrue(sess.isValid());
        sess.maxInactiveInterval(0);
        assertFalse(sess.isValid());
    }
    
    @Test
    public void testHttpSessionInvalidate() throws IOException {
        HttpSession sess = new HttpSession();
        assertTrue(sess.isValid());
        sess.invalidate();
        assertFalse(sess.isValid());
    }
    
    @After
    public void tearDown() {}
}
