package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.m2.interfaces.Response;

import org.apache.logging.log4j.Level;

public class TestHttpResponse {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    @Test
    public void testHttpResponse() throws IOException {
            
        Response res = new HttpResponse();
        res.body("happy");
        assertTrue(res.body().equals("happy"));

        String headers = res.getHeaders().toLowerCase();
        assertTrue(headers.contains("date: "));
        assertTrue(headers.contains("content-length: 5\r\n"));
        assertTrue(headers.contains("content-type: text/plain\r\n"));
        assertTrue(headers.endsWith("\r\n\r\n"));
    }
    
    @Test
    public void testHttpResponseRedirect() throws IOException {
            
        Response res = new HttpResponse();
        String headers;

        res.redirect("/best");
        assertEquals(302, res.status());

        headers = res.getHeaders().toLowerCase();
        assertTrue(headers.contains("location: /best"));

        res.redirect("/best", 301);
        assertEquals(301, res.status());

        headers = res.getHeaders().toLowerCase();
        assertTrue(headers.contains("location: /best"));
    }
    
    @Test
    public void testHttpResponseCookies() throws IOException {
            
        Response res = new HttpResponse();
        String headers;

        res.cookie("c1", "v1");
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c1=v1"));

        res.cookie("c2", "v2", 10);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c2=v2; Max-Age=10"));

        res.cookie("c3", "v3", 10, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c3=v3; Max-Age=10; Secure"));

        res.cookie("c4", "v4", 10, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c4=v4; Max-Age=10"));

        res.cookie("c5", "v5", 10, true, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c5=v5; Max-Age=10; Secure; HttpOnly"));

        res.cookie("c6", "v6", 10, false, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c6=v6; Max-Age=10; HttpOnly"));

        res.cookie("c7", "v7", 10, true, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c7=v7; Max-Age=10; Secure"));

        res.cookie("c8", "v8", 10, false, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c8=v8; Max-Age=10"));

        res.removeCookie("c1");
        res.removeCookie("c2");
        res.removeCookie("c3");
        res.removeCookie("c4");
        res.removeCookie("c5");
        res.removeCookie("c6");
        res.removeCookie("c7");
        res.removeCookie("c8");
        headers = res.getHeaders();
        assertFalse(headers.contains("Set-Cookie"));
    }
    
    @Test
    public void testHttpResponseCookiesWithPath() throws IOException {
            
        Response res = new HttpResponse();
        String headers;

        res.cookie("p1", "c1", "v1");
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c1=v1; Path=p1"));

        res.cookie("p2", "c2", "v2", 10);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c2=v2; Path=p2; Max-Age=10"));

        res.cookie("p3", "c3", "v3", 10, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c3=v3; Path=p3; Max-Age=10; Secure"));

        res.cookie("p4", "c4", "v4", 10, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c4=v4; Path=p4; Max-Age=10"));

        res.cookie("p5", "c5", "v5", 10, true, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c5=v5; Path=p5; Max-Age=10; Secure; HttpOnly"));

        res.cookie("p6", "c6", "v6", 10, false, true);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c6=v6; Path=p6; Max-Age=10; HttpOnly"));

        res.cookie("p7", "c7", "v7", 10, true, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c7=v7; Path=p7; Max-Age=10; Secure"));

        res.cookie("p8", "c8", "v8", 10, false, false);
        headers = res.getHeaders();
        assertTrue(headers.contains("Set-Cookie: c8=v8; Path=p8; Max-Age=10"));

        res.removeCookie("p1", "c1");
        res.removeCookie("p2", "c2");
        res.removeCookie("p3", "c3");
        res.removeCookie("p4", "c4");
        res.removeCookie("p5", "c5");
        res.removeCookie("p6", "c6");
        res.removeCookie("p7", "c7");
        res.removeCookie("p8", "c8");
        headers = res.getHeaders();
        assertFalse(headers.contains("Set-Cookie"));
    }

    
    @After
    public void tearDown() {}
}
